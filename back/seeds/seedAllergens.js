const mongoose = require('mongoose');

const {connection} = require('../config/db');

const Allergens = require('../models/Allergens');

const allergensArray = [
  {
    name : "Altramuz",
    photo: "/assets/logosAllergens/Altramuz.png"
  },
  {
    name : "Apio",
    photo: "/assets/logosAllergens/Apio.png"
  },
  {
    name : "Cacahuetes",
    photo: "/assets/logosAllergens/Cacahuetes.png"
  },
  {
    name : "Cacao",
    photo: "/assets/logosAllergens/Cacao.png"
  },
  {
    name : "Cereales con gluten",
    photo: "/assets/logosAllergens/Cerealescongluten.png"
  },
  {
    name : "Crustaceos",
    photo: "/assets/logosAllergens/Crustaceos.png"
  },
  {
    name : "Frutos secos",
    photo: "/assets/logosAllergens/Frutossecos.png"
  },
  {
    name : "Huevos",
    photo: "/assets/logosAllergens/Huevos.png"
  },
  {
    name : "Lacteos",
    photo: "/assets/logosAllergens/Lacteos.png"
  },
  {
    name : "Maiz",
    photo: "/assets/logosAllergens/Maiz.png"
  },
  {
    name : "Moluscos",
    photo: "/assets/logosAllergens/Moluscos.png"
  },
  {
    name : "Mostaza",
    photo: "/assets/logosAllergens/Mostaza.png"
  },
  {
    name : "Pescado",
    photo: "/assets/logosAllergens/Pescado.png"
  },
  {
    name : "Sesamo",
    photo: "/assets/logosAllergens/Sesamo.png"
  },
  {
    name : "Soja",
    photo: "/assets/logosAllergens/Soja.png"
  },
  {
    name : "Sulfitos",
    photo: "/assets/logosAllergens/Sulfitos.png"
  },
  {
    name: "Trigo",
    photo: "/assets/logosAllergens/Trigo.png"
  },
];

const allergensDocuments = allergensArray.map(item => new Allergens(item));

connection

  .then(async () => {
    const allAllergens = await Allergens.find();

    if (allAllergens.length) {
      await Allergens.collection.drop();
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
		await Allergens.insertMany(allergensDocuments);
	})
  .catch((err) => console.log(`Error creating data: ${err}`))

  .finally(() => mongoose.disconnect());