const express = require("express");
const passport = require("passport");
const bcrypt = require("bcrypt");
const User = require("../models/User");

// eslint-disable-next-line new-cap
const router = express.Router();

const authMiddleware = require("../middlewares/auth.middleware");
// const fileMiddlewares = require("../middlewares/file.middleware");

var userID = "";
// muestra listado de usuarios
router.get(
  "/",
  // [authMiddleware.isAuthenticated],
  async (req, res, next) => {
    try {
      const users = await User.find().populate("favFood").populate("allergies");
      return res.status(200).json(users);
    } catch (err) {
      next(err);
    }
  }
);

//muestra los detalles de un usuario pasado por ID
router.get(
  "/user/:id",
  // [authMiddleware.isAuthenticated],
  async (req, res, next) => {
    try {
      const userId = req.params.id;
      const user = await User.findById(userId)
        .populate("allergies")
        .populate("favFood");
      return res.status(200).json(user);
    } catch {
      next(err);
    }
  }
);

//para renderizar el form de login
router.get("/login", async (req, res, next) => {
  try {
    return res.status(200).render("login");
  } catch {
    next(err);
  }
});

//para renderizar el form de register
router.get("/register", async (req, res, next) => {
  try {
    if (!req.user) {
      return res.status(200).render("register");
    } else {
      return res.status(200).redirect("/");
    }
  } catch {
    next(err);
  }
});

//para hacer logout
router.get("/logout", (req, res, next) => {
  console.log(req.user);
  if (req.user) {
    req.logout();
    req.session.destroy(() => {
      res.clearCookie("connect.sid");
      console.log("Correct User logout");
      return res.status(200).json("Logout OK");
    });
  } else {
    console.log("No user logged");
    return res.status(200).json("No user to logout");
  }
});

//para registrar un usuario
router.post(
  "/register",
  // [fileMiddlewares.upload.single("photo")],
  // [fileMiddlewares.uploadToCloudinary],
  async (req, res, next) => {
    if (!req.user) {
      passport.authenticate("register", (error, user) => {
        if (error) {
          return res.send(error.message);
        }
        req.logIn(user, async (error) => {
          if (error) {
            return res.send(error.message);
          }
          const userId = req.user.id;
          userID = req.user.id;
          const user = await User.findById(userId);
          return res.status(200).json(user);
        });
      })(req, res, next);
    } else {
      console.log(req.user);
      console.log("Ya hay usuario logeado");
      const userId = req.user.id;
      userID = req.user.id;
      const user = await User.findById(userId);
      return res.status(200).json(user);
    }
  }
);

//para logear un usuario
router.post("/login", (req, res, next) => {
  passport.authenticate("login", (error, user) => {
    if (error) {
      return res.send(error.message);
    }

    req.logIn(user, async (error) => {
      if (error) {
        return res.send(error.message);
      }
      const userId = req.user.id;
      userID = req.user.id;
      const user = await User.findById(userId)
        .populate("allergies")
        .populate("favFood");
      res.status(200).json(user);
    });
  })(req, res, next);
});

// para actualizar un usuario, en este caso só actualizamos el campo fav
router.patch(
  "/user/:id",
  // [authMiddleware.isAuthenticated],
  // [fileMiddlewares.upload.single("photo")],
  async (req, res, next) => {
    try {
      const userId = req.params.id;
      let user = {};
      if (
        req.body.contactName ||
        req.body.contactEmail ||
        req.body.contactPhone
      ) {
        user = { contact: {} };
      }
      if (
        req.body.insuranceCompanyName ||
        req.body.insuranceCompanyPhone ||
        req.body.insuranceNumber
      ) {
        user = { insuranceCompany: {} };
      }
      if (req.body.name) {
        user.name = req.body.name;
      }
      if (req.body.phone) {
        user.phone = req.body.phone;
      }
      if (req.body.email) {
        user.email = req.body.email;
      }
      if (req.body.password) {
        const saltRounds = 10;
        const hash = await bcrypt.hash(req.body.password, saltRounds);
        user.password = hash;
      }
      if (req.body.contactName) {
        user.contact.name = req.body.contactName;
      }
      if (req.body.contactEmail) {
        user.contact.email = req.body.contactEmail;
      }
      if (req.body.contactPhone) {
        user.contact.phone = req.body.contactPhone;
      }
      if (req.body.insuranceCompanyName) {
        user.insuranceCompany.name = req.body.insuranceCompanyName;
      }
      if (req.body.insuranceCompanyPhone) {
        user.insuranceCompany.phone = req.body.insuranceCompanyPhone;
      }
      if (req.body.insuranceNumber) {
        user.insuranceCompany.insuranceNumber = req.body.insuranceNumber;
      }
      if (req.body.allergies) {
        const dbAllergies = await User.findById(userId);
        let find = false;
        req.body.allergies.map((newAllergie) => {
          if (dbAllergies.allergies.length > 0) {
            dbAllergies.allergies.map((item) => {
              if (item == newAllergie) {
                find = true;
              }
            });
            if (find === false) {
              console.log("Alergía no encontrada, se añade.");
              dbAllergies.allergies.push(newAllergie);
              user.allergies = dbAllergies.allergies;
            }
          } else {
            user.allergies = req.body.allergies;
          }
        });
      }
      if (req.body.favFood) {
        const newFavFood = await User.findById(userId);
        let find = false;
        newFavFood.favFood.map((item) => {
          if (item == req.body.favFood) {
            find = true;
          }
        });
        if (find === false) {
          console.log("Alimento no encontrado, se añade.");
          newFavFood.favFood.push(req.body.favFood);
          user.favFood = newFavFood.favFood;
        }
      }
      if (req.body.photo) {
        user.photo = req.body.photo;
      }

      if (req.body.evaluation) {
        user.evaluation = req.body.evaluation;
      }

      const updatedUser = await User.findByIdAndUpdate(userId, user, {
        new: true,
      });

      // si usuario está vacio, significa q no hay nada q actualziar
      if (Object.keys(user).length === 0 && user.constructor === Object) {
        console.log("Ningún campo se ha actualizado.");
      } else {
        console.log("Usuario actualizado correctamente.");
        console.log("Campos actualizados: ", user);
      }
      return res.status(200).json(updatedUser);
    } catch (err) {
      next(err);
    }
  }
);

//para eliminar un usuario.
router.delete(
  "/user/:id",
  // [authMiddleware.isAuthenticated],
  async (req, res, next) => {
    try {
      const userId = req.params.id;
      req.session.destroy(() => {
        res.clearCookie("connect.sid");
        console.log("Correct User logout");
      });
      await User.findByIdAndDelete(userId);
      console.log("User Deleted!");
      return res.status(200).json("User Deleted!");
    } catch (err) {
      next(err);
    }
  }
);
router.get(
  "/user/allergen/:id",
  // [authMiddleware.isAuthenticated],
  async (req, res, next) => {
    try {
      const allergenId = req.params.id;
      const userId = userID;
      const dataUser = await User.findById(userId);
      const findAllergen = dataUser.allergies.find(
        (item) => item == allergenId
      );
      if (findAllergen) {
        const filterAllergen = dataUser.allergies.filter(
          (item) => item != findAllergen
        );
        const newDataUser = await User.findByIdAndUpdate(
          userId,
          { allergies: filterAllergen },
          { new: true }
        ).populate("allergies");
        return res.status(200).json(newDataUser);
      }
      return res.status(304).json(dataUser);
    } catch (err) {
      next(err);
    }
  }
);

router.get(
  "/user/favFood/:id",
  // [authMiddleware.isAuthenticated],
  async (req, res, next) => {
    try {
      const favFoodId = req.params.id;
      const userId = userID;
      const dataUser = await User.findById(userId);
      const findFavFood = dataUser.favFood.find((item) => item == favFoodId);
      if (findFavFood) {
        const filterFavFood = dataUser.favFood.filter(
          (item) => item != findFavFood
        );
        const newDataUser = await User.findByIdAndUpdate(
          userId,
          { favFood: filterFavFood },
          { new: true }
        ).populate("favFood");
        return res.status(200).json(newDataUser);
      }
      return res.status(304).json(dataUser);
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
