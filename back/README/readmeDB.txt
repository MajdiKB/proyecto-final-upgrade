Base de Datos MongoDB:

Entidades:

  users: {
    id: (auto),
    name: string,
    email: string,
    phone: number,
    photo: string,
    fav: idFood[], --> para guardar los ids de los alimentos favoritos del usuario
    password: string,
    contact: { --> para guardar los datos del contacto para llamarlo/a en caso de emergencia.
      name: string,
      email: string,
      phone: string,
    }
    insuranceCompany: {
      name: string,
      phone: number,
      insuranceNumber: string,
    }
    allergies: idAllergens[], --> para guardar los ids de los alergenos del usuario
  }

  food: {
    id: (auto),
    name: string,
    photo: string,
    description: string,
    allergens: idAllergens[], --> para guardar los id de los alergenos q contiene el alimento
  }

  allergens: {
    id: (auto),
    name: string,
    photo: string,
  }

  listFood: {
    id: (auto),
    idUser: string, --> guardamos el id del usuario para saber q esta lista pertenece a ese usuario,
    idFood: idFood [], --> guardamos los id de los alimentos q componen la lista.
    description: string, --> para que el usuario pueda poner cualquier anotación en cada lista.
    date: date, --> fecha de creación.
  }

  experiencie: {
    id: (auto),
    idUser: string, --> guardamos el id del usuario para saber q esta experiencia corresponde a ese usuario,
    experience: number (1 (low) to 5 (high)),
    description: string,
  }



