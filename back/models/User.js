const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
      name: { type: String, required: true  },
      email: { type: String, required: true },
      password: { type: String, required: true },
      phone: { type: Number },
      photo: { type: String },
      contact: {
        name: { type: String },
        email: { type: String },
        phone: { type: String },
      },
      insuranceCompany: {
        name: { type: String },
        phone: { type: Number },
        insuranceNumber: { type: String },
      },
      evaluation: {type: String },
      favFood: [{ type: mongoose.Types.ObjectId, ref: 'Food' }],
      allergies: [{ type: mongoose.Types.ObjectId, ref: 'Allergens' }],
    },
    {
      timestamps: true,
    },
);

const User = mongoose.model('User', userSchema);
module.exports = User;
