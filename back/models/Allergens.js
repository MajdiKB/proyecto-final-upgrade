const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const allergensSchema = new Schema(
  {
    name: { type: String, required: true  },
    photo: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

const Allergens = mongoose.model('Allergens', allergensSchema);
module.exports = Allergens;