# applergic

Welcome to the final project of the Bootcamp "Full Stack Web Developer".

## Clone the repository in local folder.

`git@gitlab.com:MajdiKB/proyecto-final-upgrade.git`


## Install dependencies:

In the folder "front" (cd front)

`npm i`

## Run app:

`npm start`
