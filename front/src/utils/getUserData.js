import getUserApi from "../api/getUser";

const getUserData = async (userId) => {
  try {
    const data = await getUserApi(userId);
    return data;
  } catch (error) {
    console.log("Error al traer datos del usuario.");
  }
};

export default getUserData;