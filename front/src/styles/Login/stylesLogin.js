import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  loginButtom: {
    background: 'rgba(38,199,220,.8)',
    border: 0,
    borderRadius: 8,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    height: 40,
    margin: '2%',
    width: 220,
    display: 'flex',
  },
  registerButtom: {
    background: '#FA7F9B',
    border: 0,
    borderRadius: 8,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white !Important',
    height: 40,
    width: 220,
    display: 'flex',
    margin: '2%',
  },
  
}));

export default useStyles;