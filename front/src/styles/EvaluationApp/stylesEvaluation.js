import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },

  image: {
    marginTop: theme.spacing(8),
    
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default useStyles;

