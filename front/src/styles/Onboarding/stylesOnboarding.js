import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({

  img: {
    display: "block",
    overflow: "hidden",
    width: "100%",
  },
}));

export default useStyles;