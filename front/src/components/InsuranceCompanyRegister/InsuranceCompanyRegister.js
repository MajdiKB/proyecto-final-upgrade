import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import routes from "../../config/routes";
import { useHistory, Link } from "react-router-dom";
import updateUser from "../../api/updateUser";
import { authSelector } from "../../store/auth";
import Copyright from "../Copyright";
import getUserData from "../../utils/getUserData";
// Material ui
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
// Styles
import useStyles from "../../styles/InsuranceCompanyRegister/stylesInsuranceCompanyRegister";

const InsuranceCompanyRegister = () => {
  const classes = useStyles();
  const history = useHistory();
  const [dataUser, setDataUser] = useState();

  const emptyContact = {
    insuranceCompanyName: dataUser?.insuranceCompany?.name || "",
    insuranceCompanyPhone: dataUser?.insuranceCompany?.phone || "",
    insuranceNumber: dataUser?.insuranceCompany?.insuranceNumber || "",
  };
  const [userFormData, setUserFormData] = useState(emptyContact);
  const { userId } = useSelector(authSelector);

  useEffect(() => {
    getData();
    return () => {};
  }, []);

  const getData = async () => {
    const data = await getUserData(userId);
    setDataUser(data);
  };

  const handleChangeInput = (event) => {
    setUserFormData((prevValue) => ({
      ...prevValue,
      [event.target.name]: event.target.value,
    }));
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    doUpdate();
  };

  useEffect(() => {
    if (userFormData.insuranceCompanyName === "") {
      setUserFormData((prevValue) => ({
        ...prevValue,
        insuranceCompanyName: emptyContact.insuranceCompanyName,
      }));
    }
    if (userFormData.insuranceCompanyPhone === "") {
      setUserFormData((prevValue) => ({
        ...prevValue,
        insuranceCompanyPhone: emptyContact.insuranceCompanyPhone,
      }));
    }
    if (userFormData.insuranceNumber === "") {
      setUserFormData((prevValue) => ({
        ...prevValue,
        insuranceNumber: emptyContact.insuranceNumber,
      }));
    }
    return () => {};
  }, [dataUser]);

  const doUpdate = async () => {
    try {
      if (
        userFormData.insuranceCompanyName === "" &&
        userFormData.insuranceCompanyPhone === "" &&
        userFormData.insuranceNumber === ""
      ) {
        await updateUser(userFormData, userId);
        history.push(routes.user);
      } else {
        const data = await updateUser(userFormData, userId);
        if (data.evaluation === undefined) {
          history.push(routes.allAllergens, data);
        } else {
          history.push(routes.user);
        }
      }
    } catch (error) {
      console.log("Error al actualizar.");
    }
  };

  return (
    <div className="Register">
      <Button component={Link} to={routes.user} size="small">
        <KeyboardArrowLeft />
        Volver a Mi Perfil
      </Button>

      <div className="TitleContact">
        <h1>Vamos a añadir los datos de tu compañía de seguros.</h1>
        <h3>Nos pondremos en contacto con tu seguro en caso de emergencia.</h3>
      </div>

      <form className="Form" onSubmit={handleFormSubmit} noValidate>
        <TextField
          variant="standard"
          margin="normal"
          fullWidth
          id="insuranceCompanyName"
          label="Compañía de Seguros"
          name="insuranceCompanyName"
          autoComplete="insuranceCompanyName"
          autoFocus
          value={userFormData.insuranceCompanyName}
          onChange={handleChangeInput}
        />
        <TextField
          variant="standard"
          margin="normal"
          fullWidth
          id="insuranceCompanyPhone"
          label="Teléfono"
          name="insuranceCompanyPhone"
          type="number"
          inputProps={{ maxLength: 9 }}
          autoComplete="phone"
          value={userFormData.insuranceCompanyPhone}
          onChange={handleChangeInput}
        />
        <TextField
          variant="standard"
          margin="normal"
          fullWidth
          name="insuranceNumber"
          label="Nº Póliza"
          id="insuranceNumber"
          type="text"
          value={userFormData.insuranceNumber}
          onChange={handleChangeInput}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
        >
          Guardar Datos
        </Button>

        <Link className="ContactLink" to={routes.home}>
          Registraré mi compañía de seguros en otro momento
        </Link>
      </form>

      <div className="CopyRight">
        <Copyright />
      </div>
    </div>
  );
};

export default InsuranceCompanyRegister;
