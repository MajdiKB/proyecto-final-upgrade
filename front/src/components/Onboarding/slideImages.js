export const slideImages = [
  {
    imgPath: "/assets/onboarding/scan2.png",
    label:
      "¡Bienvenido a Applergic! Contamos con un base de datos de alimentos y Applergic te dirá si son aptos para ti.",
  },
  {
    imgPath: "/assets/onboarding/ambulancia.png",
    label:
      "En caso de emergencia nos pondremos en contacto con la persona y/o compañía de seguros que nos digas.",
  },
];