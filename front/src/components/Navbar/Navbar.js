import React from "react";
import { useDispatch, useSelector } from "react-redux";
import routes from "../../config/routes";
import { Link, useHistory } from "react-router-dom";
import { resetAuthUser } from "../../store/auth";
import getUserApi from "../../api/getUser";
import { authSelector } from "../../store/auth";
import logoutUserApi from "../../api/logoutUser";

// Material ui
import Button from "@material-ui/core/Button";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FastfoodIcon from "@material-ui/icons/Fastfood";
import PersonIcon from "@material-ui/icons/Person";
import HomeIcon from "@material-ui/icons/Home";
import MenuIcon from "@material-ui/icons/Menu";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import CallIcon from '@material-ui/icons/Call';
// Styles
import useStyles from "../../pages/Home/HomeStyles";
import StyledMenu from "../../styles/Navbar/StyledMenu";
import StyledMenuItem from "../../styles/Navbar/StyledMenuItem;";


const Navbar = () => {
  const { userId } = useSelector(authSelector);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const dispatch = useDispatch();
  const history = useHistory();

  const logout = async (userId) => {
    try {
      const data = await getUserApi(userId);
      // console.log(data);
      if (data.evaluation===undefined ){
        history.push(routes.evaluation)
      }
      else{
        sessionStorage.removeItem("token_applergic");
        sessionStorage.removeItem("name_user_applergic");
        dispatch(resetAuthUser());
        await setAnchorEl(null);
        logOutUserInApi();
        history.push(routes.login);
      }
    } catch (error) {
      console.log("Error al hacer logOut.");
    }
  };

  const logOutUserInApi = async () => {
    await logoutUserApi();
    // console.log(data);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const classes = useStyles();

  return (
    <div>
      <Button
        aria-controls="customized-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MenuIcon className={classes.homeIcon}></MenuIcon>
      </Button>
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <Link to={routes.home} onClick={handleClose}>
          <StyledMenuItem>
            <ListItemIcon>
              <HomeIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="HOME" />
          </StyledMenuItem>
        </Link>

        <Link to={routes.user} onClick={handleClose}>
          <StyledMenuItem>
            <ListItemIcon>
              <PersonIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="Perfil" />
          </StyledMenuItem>
        </Link>

        <Link to={routes.favFood} onClick={handleClose}>
          <StyledMenuItem>
            <ListItemIcon>
              <FastfoodIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="Mis Alimentos" />
          </StyledMenuItem>
        </Link>

        <Link to={routes.userAllergens} onClick={handleClose}>
          <StyledMenuItem>
            <ListItemIcon>
              <FavoriteIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="Mis Alergias" />
          </StyledMenuItem>
        </Link>

        <Link to={routes.sos} onClick={handleClose}>
          <StyledMenuItem>
            <ListItemIcon>
              <CallIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="S.O.S" />
          </StyledMenuItem>
        </Link>

        <StyledMenuItem>
          <ListItemIcon>
            <ExitToAppIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText
            primary="Salir"
            onClick={() => {
              logout(userId);
            }}
          />
        </StyledMenuItem>
      </StyledMenu>
    </div>
  );
};

export default Navbar;
