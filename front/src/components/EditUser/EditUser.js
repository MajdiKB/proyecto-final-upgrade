import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import routes from "../../config/routes";
import { useHistory, Link } from "react-router-dom";
import updateUser from "../../api/updateUser";
import { authSelector } from "../../store/auth";
import Copyright from "../Copyright";
import getUserData from "../../utils/getUserData";
import { useDispatch } from "react-redux";
import { setAuthUser } from "../../store/auth";
// Material ui
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import Avatar from "@material-ui/core/Avatar";
// Styles
import "../../styles/UserContactRegister/stylesUserContactRegister.scss";
import useStyles from "../../styles/InsuranceCompanyRegister/stylesInsuranceCompanyRegister";

const EditUser = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const history = useHistory();
  const [dataUser, setDataUser] = useState();
  const [responsePhoto, setResponsePhoto] = useState(false);

  const emptyContact = {
    name: dataUser?.name || "",
    phone: dataUser?.phone || "",
    email: dataUser?.email || "",
    password: dataUser?.password || "",
    photo: dataUser?.photo || "",
  };
  const [userFormData, setUserFormData] = useState(emptyContact);
  const { userId } = useSelector(authSelector);
  const uploadedImage = React.useRef(null);
  const imageUploader = React.useRef(null);

  useEffect(() => {
    getData();
    return () => {};
  }, []);

  const getData = async () => {
    const data = await getUserData(userId);
    setDataUser(data);
  };

  const handleChangeInput = (event) => {
    setUserFormData((prevValue) => ({
      ...prevValue,
      [event.target.name]: event.target.value,
    }));
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    doUpdate();
  };

  const handleImageUpload = async (e) => {
    const [file] = e.target.files;
    if (file) {
      const reader = new FileReader();
      const { current } = uploadedImage;
      current.file = file;
      reader.onload = (e) => {
        current.src = e.target.result;
      };
      reader.readAsDataURL(file);
      // console.log(file);
      await uploadImage(e);
    }
  };

  const uploadImage = async (e) => {
    const files = e.target.files;
    const data = new FormData();
    data.append("file", files[0]);
    data.append("upload_preset", "ml_default");
    //controlar cuando se sube la foto
    const res = await fetch(
      "https://api.cloudinary.com/v1_1/dj0zfm4sd/image/upload",
      { method: "POST", body: data }
    );
    const file = await res.json();
    if (file.bytes <= 50000) {
      if (file.secure_url !== undefined) {
        setUserFormData((prevValue) => ({
          ...prevValue,
          [e.target.name]: file.secure_url,
        }));
        setResponsePhoto(false);
      }
    } else {
      alert("Seleccione una foto con un tamaño menor (MÁX 50 KB)");
      setResponsePhoto(true);
    }
  };

  useEffect(() => {
    if (userFormData.name === "") {
      setUserFormData((prevValue) => ({
        ...prevValue,
        name: emptyContact.name,
      }));
    }
    if (userFormData.phone === "") {
      setUserFormData((prevValue) => ({
        ...prevValue,
        phone: emptyContact.phone,
      }));
    }
    if (userFormData.phone === "") {
      setUserFormData((prevValue) => ({
        ...prevValue,
        email: emptyContact.email,
      }));
    }
    // if (userFormData.password === "") {
    //   setUserFormData((prevValue) => ({
    //     ...prevValue,
    //     password: emptyContact.password,
    //   }));
    // }
    return () => {};
  }, [dataUser]);

  const doUpdate = async () => {
    try {
      const data = await updateUser(userFormData, userId);
      dispatch(setAuthUser({ id: data._id, name: data.name }));
      history.push(routes.user, data);
    } catch (error) {
      console.log("Error al actualizar.");
    }
  };

  return (
    <div className="Register">
      <Button component={Link} to={routes.user} size="small">
        <KeyboardArrowLeft />
        Volver a Mi Perfil
      </Button>

      <div className="TitleContact">
        <h1>{dataUser?.name}</h1>
        {dataUser?.photo ? (
          <div>
            <img
              className="user-data__photo"
              src={dataUser.photo}
              alt={dataUser.name}
            ></img>
          </div>
        ) : (
          <Avatar />
        )}
        <h3>
          ¿Quieres cambiar tu nombre, teléfono, foto de perfil o contraseña en
          Applergic?
        </h3>
      </div>

      <form className="Form" onSubmit={handleFormSubmit} noValidate>
        <div>
          <h5>Cambiar foto de perfil</h5>
          <span>
            {/* <Avatar> */}
            {/* este era el componente photo */}
            {imageUploader && (
              <div onClick={() => imageUploader.current.click()}>
                <img className="logo-photo" ref={uploadedImage} alt="" />
                <input
                  type="file"
                  accept="image/*"
                  name="photo"
                  id="photo"
                  onChange={handleImageUpload}
                  ref={imageUploader}
                  style={{
                    display: "none",
                  }}
                />
              </div>
            )}

            {/* hasta aqui llega componente photo */}
            {/* </Avatar> */}
          </span>
        </div>
        <TextField
          variant="standard"
          margin="normal"
          fullWidth
          id="name"
          label="Nombre"
          name="name"
          autoComplete="name"
          autoFocus
          value={userFormData.name}
          onChange={handleChangeInput}
        />
        <TextField
          variant="standard"
          margin="normal"
          fullWidth
          name="email"
          label="Email"
          id="email"
          helperText="No se puede modificar"
          type="text"
          value={userFormData.email}
          disabled
        />
        <TextField
          variant="standard"
          margin="normal"
          fullWidth
          id="phone"
          label="Teléfono"
          name="phone"
          type="number"
          inputProps={{ maxLength: 9 }}
          autoComplete="phone"
          value={userFormData.phone}
          onChange={handleChangeInput}
        />
        <TextField
          variant="standard"
          margin="normal"
          fullWidth
          name="password"
          label="Nueva Contraseña"
          id="password"
          helperText="Si no se modifica, se mantiene la contraseña actual"
          type="password"
          value={userFormData.password}
          onChange={handleChangeInput}
        />
        {responsePhoto === false && (
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            ACTUALIZAR DATOS
          </Button>
        )}
      </form>
    </div>
  );
};

export default EditUser;
