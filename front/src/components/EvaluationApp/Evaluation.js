import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import routes from "../../config/routes";

import { authSelector, resetAuthUser } from "../../store/auth";
import Copyright from "../Copyright";
import updateUser from "../../api/updateUser";
import Navbar from "../Navbar";
import logoutUserApi from "../../api/logoutUser";
// Material ui
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import Container from "@material-ui/core/Container";
import { CardMedia } from "@material-ui/core";
import { CardActionArea } from "@material-ui/core";
// Styles
import "../../styles/EvaluationApp/stylesEvaluation.scss";
import useStyles from "../../styles/EvaluationApp/stylesEvaluation";

const Evaluation = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const { userId, userName } = useSelector(authSelector);

  const [userFormData, setUserFormData] = useState(2);

  const logOutUserInApi = async () => {
    await logoutUserApi();
  };

  const doUpdate = async (userFormData) => {
    try {
      await updateUser(userFormData, userId);
      sessionStorage.removeItem("token_applergic");
      sessionStorage.removeItem("name_user_applergic");
      dispatch(resetAuthUser());
      logOutUserInApi();
      // console.log("Logout User OK!");
      history.push(routes.login);
    } catch (error) {
      console.log("Error al actualizar.");
    }
  };

  const handleFormSubmit = async () => {
    doUpdate({ evaluation: userFormData });
  };

  return (
    <div className="main">
        <header className="navbar">
          <span>
            <Navbar></Navbar>
          </span>
          <span>Hasta la próxima {userName}!</span>
        </header>
        <CssBaseline />
        <Button component={Link} to={routes.home} size="small">
          <KeyboardArrowLeft />
          volver
        </Button>
        <Grid className={classes.image} />
        <Grid container direction="row" justify="center" alignItems="center">
          <CardActionArea>
          <div className="img-container">
            <CardMedia
              component="img"
              alt="Applergic"
              height="100%"
              image="/assets/starrating/logoApplergicFigurasGiro.png"
              title="Ok"
            />
            </div>
          </CardActionArea>
        </Grid>
        <Typography variant="h5" gutterBottom>
          ¡Gracias por usar Applergic!
        </Typography>
        <Typography variant="h5" gutterBottom>
          Por favor, evalua tu experiencia.
        </Typography>
        <Box component="fieldset" borderColor="transparent">
          <Typography component="legend"></Typography>
          <Rating
            name="evaluation"
            id="evaluation"
            value={userFormData}
            onChange={(_, newValue) => {
              setUserFormData(newValue);
            }}
          />
        </Box>
        <Button onClick={handleFormSubmit} color="primary">
          ENVIAR
        </Button>
    </div>
  );
};

export default Evaluation;
