import React from 'react';
import routes from "../../config/routes";
import { Link } from "react-router-dom";
// Material ui
import Typography from "@material-ui/core/Typography";


const Copyright = () => {
  return (
    <Typography variant="body2" color="primary" align="center">
      {"Copyright © "}
      <Link color="inherit" to={routes.home}>
        APPlecgic
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
};

export default Copyright;