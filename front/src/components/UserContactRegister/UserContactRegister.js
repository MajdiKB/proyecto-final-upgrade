import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import routes from "../../config/routes";
import { useHistory, Link } from "react-router-dom";
import updateUser from "../../api/updateUser";
import { authSelector } from "../../store/auth";
import getUserData from "../../utils/getUserData";
// External Components
import Copyright from "../Copyright";
// Material ui
import Button from "@material-ui/core/Button";
import { TextValidator, ValidatorForm} from 'react-material-ui-form-validator';

import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
// Styles
import useStyles from "../../styles/UserContactRegister/stylesUserContactRegister";
import "../../styles/UserContactRegister/stylesUserContactRegister.scss";

const UserContactRegister = () => {
  const classes = useStyles();
  const history = useHistory();
  const { userId } = useSelector(authSelector);
  const [dataUser, setDataUser] = useState();
  const emptyContact = {
    contactName: dataUser?.contact?.name || "",
    contactEmail: dataUser?.contact?.email || "",
    contactPhone: dataUser?.contact?.phone || "",
  };
  const [userFormData, setUserFormData] = useState(emptyContact);

  useEffect(() => {
    getData();
    return () => {};
  }, []);

  const getData = async () => {
    const data = await getUserData(userId);
    setDataUser(data);
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    doUpdate(userFormData);
  };

  const handleChangeInput = (event) => {
    setUserFormData((prevValue) => ({
      ...prevValue,
      [event.target.name]: event.target.value,
    }));
  };

  useEffect(() => {
    if (userFormData.contactName === "") {
      setUserFormData((prevValue) => ({
        ...prevValue,
        contactName: emptyContact.contactName,
      }));
    }
    if (userFormData.contactEmail === "") {
      setUserFormData((prevValue) => ({
        ...prevValue,
        contactEmail: emptyContact.contactEmail,
      }));
    }
    if (userFormData.contactPhone === "") {
      setUserFormData((prevValue) => ({
        ...prevValue,
        contactPhone: emptyContact.contactPhone,
      }));
    }
    return () => {};
  }, [dataUser]);

  const doUpdate = async () => {
    try {
      if (
        userFormData.contactEmail === "" &&
        userFormData.contactName === "" &&
        userFormData.contactPhone === ""
      ) {
        await updateUser(userFormData, userId);
        history.push(routes.user);
      } else {
        const data = await updateUser(userFormData, userId);
        if (data.evaluation === undefined) {
          history.push(routes.insuranceCompanyRegister, data);
        } else {
          history.push(routes.user);
        }
      }
    } catch (error) {
      console.log("Error al actualizar.");
    }
  };

  return (
    <div className="Register">
        <Button component={Link} to={routes.user} size="small">
          <KeyboardArrowLeft />
          Volver a Mi Perfil
        </Button>
      <div className="TitleContact">
        <h1>Vamos a añadir los datos de contacto de emergencia.</h1>
        <h3>
          Nos pondremos en contacto con tu persona de confianza en caso de
          emergencia.
        </h3>
      </div>

      <ValidatorForm className="Form" onSubmit={handleFormSubmit} noValidate>
        <TextValidator
          variant="standard"
          margin="normal"
          fullWidth
          id="contactName"
          label="Nombre:"
          name="contactName"
          autoComplete="contactName"
          autoFocus
          value={userFormData.contactName}
          onChange={handleChangeInput}
        />
        <TextValidator
          variant="standard"
          margin="normal"
          fullWidth
          id="contactEmail"
          label="Email"
          name="contactEmail"
          type="email"
          autoComplete="email"
          validators={['isEmail']}
          errorMessages={['El email introducido no es válido']}
          value={userFormData.contactEmail}
          onChange={handleChangeInput}
        />
        <TextValidator
          variant="standard"
          margin="normal"
          fullWidth
          name="contactPhone"
          label="Teléfono"
          id="contactPhone"
          type="number"
          value={userFormData.contactPhone}
          onChange={handleChangeInput}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
        >
          Guardar Datos
        </Button>

        <Link className="ContactLink" to={routes.insuranceCompanyRegister}>
          Registraré mi contacto de emergencia en otro momento
        </Link>
        </ValidatorForm>

      <div className="CopyRight">
        <Copyright />
      </div>
    </div>
  );
};

export default UserContactRegister;
