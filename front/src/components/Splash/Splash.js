import React from "react";
import { Link } from "react-router-dom";
import routes from "../../config/routes";
// Styles
import "../../styles/Splash/Splash.scss";

const Splash = () => {
  return (
    <div className="Splash">
      <div className="Title">
        <h1 className="Title1">Applergic</h1>
        <h3>Mi guía alimentaria</h3>
      </div>
      <div className="Img">
        <Link to={routes.onboarding}>
          <img
            src={process.env.PUBLIC_URL + "/assets/onboarding/logoApplergicFigurasGiro.png"}
            alt="splash logo"
          />
        </Link>
      </div>
    </div>
  );
};

export default Splash;
