const splash = "/";
const home = "/home";
const user = "/user";
const sos = "/sos";
const userRegister = `${user}/register`;
const contactRegister = `${user}/contactregister`;
const insuranceCompanyRegister = `${user}/insurancecompanyregister`;
const allAllergens = "/allAllergens"
const userAllergens = `${user}/userAllergens`;
const allFood = "/allFood";
const favFood = `${user}/favfood`;
const login = `${user}/login`;
const onboarding = "/onboarding";
const okregister = `${user}/okregister`;
const evaluation = `${user}/evaluation`;
const editUser=`${user}/edituser`;

const routes = {
  splash,
  home,
  user,
  sos,
  userRegister,
  contactRegister,
  insuranceCompanyRegister,
  userAllergens,
  favFood,
  login,
  onboarding,
  okregister,
  evaluation,
  allFood,
  allAllergens,
  editUser,
};

export default routes;
