import apiClient from "./api";
// const BACK = process.env.BACK || 4000;
const appHeroku = process.env.REACT_APP_HEROKU || process.env.REACT_APP_LOCAL;

const allergensUrl = `${appHeroku}/allergens/`;

const allergensApi = async () => {
  const response = await apiClient(`${allergensUrl}`, {
    headers: {
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    // credentials: "include",
  }).then(
    (response) => response
  );
  return response;
};

export default allergensApi;
