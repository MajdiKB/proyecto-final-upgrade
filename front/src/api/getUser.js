import apiClient from "./api";

// const BACK = process.env.BACK || 4000;
const appHeroku = process.env.REACT_APP_HEROKU || process.env.REACT_APP_LOCAL;
const userUrl = `${appHeroku}/users/user`;

const getUserApi = async (id) => {
  const response = await apiClient(`${userUrl}/${id}`, {
    headers: {
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    // credentials: "include",
  }).then((response) => response);
  console.log('respuesta en api de usuario', response);
  return response;
};

export default getUserApi;
