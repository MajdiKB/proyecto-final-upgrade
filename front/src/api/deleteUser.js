import apiClient from "./api";

// const BACK = process.env.BACK || 4000;
const appHeroku = process.env.REACT_APP_HEROKU || process.env.REACT_APP_LOCAL;
const deleteUserUrl = `${appHeroku}/users/user/`;

const deleteUserApi = async (id) => {
  const url = deleteUserUrl + id;
  const response = await apiClient(url, {
    method: "DELETE",
    headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  },
  // credentials: "include",
  }).then((response) => response);
  return response;
};

export default deleteUserApi;