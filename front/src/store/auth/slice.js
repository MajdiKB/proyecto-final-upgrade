import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  userId: sessionStorage.getItem("token_applergic") ?? undefined,
  userName: sessionStorage.getItem("name_user_applergic") ?? undefined,
  error: undefined,
};
const {
  actions: {
    setAuthUser,
    setError,
    resetAuthUser,
  },
  reducer,
  name: authName
} = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setAuthUser: (state, action) => {
      return {...state, userId: action.payload.id, userName: action.payload.name};
    },
    setError: (state, {payload}) => {
      return {...state, error: payload };
    },
    resetAuthUser: () => {
      return {initialState};
    },
  }
});

const resetAll = () => {
  return (dispatch) => {
    dispatch(resetAuthUser());
  }
};

const authSelector = (state) => state[authName];

export {
  authName,
  authSelector,
  setAuthUser,
  setError,
  resetAuthUser,
  resetAll,
}

export default reducer;