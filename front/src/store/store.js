import {combineReducers, configureStore} from '@reduxjs/toolkit';

import auth, {authName} from './auth';
import foodAllergens, {foodAllergensName} from './foodAllergens';

const store = configureStore({
  reducer: combineReducers({
    [authName]: auth,
    [foodAllergensName]: foodAllergens,
  }),
});

export default store;
