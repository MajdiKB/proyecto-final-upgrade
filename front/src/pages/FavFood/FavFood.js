import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import getUserApi from "../../api/getUser";
import allergensApi from "../../api/allergens";
import deleteFavFoodUserApi from "../../api/deleteFavFood";
import { useSelector } from "react-redux";
import { authSelector } from "../../store/auth";
import routes from "../../config/routes";
// External Components
import Navbar from "../../components/Navbar";
// Matrial ui
import Paper from "@material-ui/core/Paper";
import { Container } from "@material-ui/core";
import { Button } from "@material-ui/core";
// Styles
import useStyles from "../Sos/style";
import "../../styles/FavFoodPage/stylesFavFood.scss";


const FavFood = () => {
  const classes = useStyles();
  const { userId, userName } = useSelector(authSelector);
  const [allergens, setAllergens] = useState([]);
  const [userFood, setUserFood] = useState([]);

  useEffect(() => {
    getAllAllergens();
    getUser(userId);
    return () => {};
  }, [userId]);

  const getUser = async (userId) => {
    try {
      const data = await getUserApi(userId);
      setUserFood(data.favFood);
    } catch (error) {
      console.log("Error al traer usuario.");
    }
  };
  const getAllAllergens = async () => {
    try {
      const allergensData = await allergensApi();
      setAllergens(allergensData);
    } catch (error) {
      console.log("Error al traer alérgenos.");
    }
  };

  const deleteFavFood = async (favFoodId) => {
    try {
      const data = await deleteFavFoodUserApi(favFoodId);
      setUserFood(data.favFood);
    } catch (error) {
      console.log("Error al eliminar alimento del usuario.");
    }
  };

  return (
    <div className="FoodContainer">
      <header className="AllergensNavbar">
        <span>
          <Navbar />
        </span>
        <span>¡Hola {userName}!</span>
      </header>
      {userFood.length > 0 ? (
        <div>
          <h3 className="title">Lista de alimentos de {userName}</h3>
          <Container className="user-data">
            {userFood.map((food) => {
              return (
                <Paper square className={classes.paper} key={food._id}>
                  <h3 className="favFood-container__items-item">
                    {" "}
                    {food.name}{" "}
                  </h3>
                  <img
                    className="photo-allergen-big"
                    src={food.photo}
                    alt={food.name}
                  ></img>
                  <h4 className="favFood-container__items-item">
                    Composición:
                  </h4>
                  <p>{food.composition}</p>
                  {food.allergens && (
                    <div>
                      <h4 className="favFood-container__items-item">
                        Alérgenos:
                      </h4>
                      {food.allergens.map((allergenFood) => {
                        return (
                          <div key={allergenFood}>
                            {allergens.map((allergen) => {
                              return (
                                <div key={allergen._id}>
                                  {allergen._id === allergenFood && (
                                    <div>
                                      <p>{allergen.name}</p>
                                      <img
                                        className="photo-allergen favFood-container__items-item"
                                        src={allergen.photo}
                                        alt={allergen.name}
                                      ></img>
                                    </div>
                                  )}
                                </div>
                              );
                            })}
                          </div>
                        );
                      })}
                      <Button
                        color="primary"
                        onClick={() => {
                          deleteFavFood(food._id);
                        }}
                      >
                        Eliminar alimento
                      </Button>
                    </div>
                  )}
                </Paper>
              );
            })}
          </Container>
        </div>
      ) : (
        <div className="no-food-container">
          <h3>Aún no tiene alimentos en su lista.</h3>
          <div className="LogoSos">
            <img
              src={process.env.PUBLIC_URL + "/assets/onboarding/logo.png"}
              alt="Applergic logo"
            />
          </div>
        </div>
      )}

      <Button
        component={Link}
        to={routes.allFood}
        className="FoodButton"
        type="submit"
        variant="contained"
        color="primary"
      >
        Añadir alimentos a tu lista de favoritos
      </Button>
    </div>
  );
};

export default FavFood;
