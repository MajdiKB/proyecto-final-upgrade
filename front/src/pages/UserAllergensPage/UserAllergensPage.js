import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import getUserApi from "../../api/getUser";
import deleteAllergenUserApi from "../../api/deleteAllergen";
import { useSelector } from "react-redux";
import { authSelector } from "../../store/auth";
import routes from "../../config/routes";
// External Components
import Navbar from "../../components/Navbar";
// Material ui
import { Container } from "@material-ui/core";
import { Paper } from "@material-ui/core";
import { Button } from "@material-ui/core";
// Styles
import useStyles from "../Sos/style";
import "../../styles/UserAllergensPage/stylesUserAllergensPage.scss";



const UserAllergensPage = () => {
  const classes = useStyles();
  const { userId, userName } = useSelector(authSelector);
  const [userAllergens, setUserAllergens] = useState([]);

  useEffect(() => {
    getUser(userId);
    return () => {};
  }, [userId]);

  const getUser = async (userId) => {
    try {
      const data = await getUserApi(userId);
      setUserAllergens(data.allergies);
    } catch (error) {
      console.log("Error al traer usuario.");
    }
  };

  const deleteAllergen = async (allergenId) => {
    try {
      const data = await deleteAllergenUserApi(allergenId);
      setUserAllergens(data.allergies);
    } catch (error) {
      console.log("Error al eliminar alérgeno del usuario.");
    }
  };

  return (
    <div className="FoodContainer">
      <header className="AllergensNavbar">
        <span>
          <Navbar />
        </span>
        <span>¡Hola {userName}!</span>
      </header>
      {userAllergens.length > 0 ? (
        <div>
          <h4>Lista de alergias e intolerancias de {userName}</h4>
          <Container className="user-data">
            {userAllergens.map((allergie) => {
              return (
                <Paper square className={classes.paper} key={allergie._id}>
                  <h3 className="favFood-container__items-item">
                    {" "}
                    {allergie.name}{" "}
                  </h3>
                  <img
                    className="photo-allergen"
                    src={allergie.photo}
                    alt={allergie.name}
                  ></img>
                  <Button
                    color="primary"
                    onClick={() => {
                      deleteAllergen(allergie._id);
                    }}
                  >
                    Eliminar alergia
                  </Button>
                </Paper>
              );
            })}
          </Container>
        </div>
      ) : (
        <div className="no-allergens-container">
          <h3>Configure sus alergias e intolerancias.</h3>
          <div className="LogoSos">
            <img
              src={process.env.PUBLIC_URL + "/assets/onboarding/logo.png"}
              alt="Applergic logo"
            />
          </div>
        </div>
      )}

      <Button
        component={Link}
        to={routes.allAllergens}
        className="FoodButton"
        type="submit"
        variant="contained"
        color="primary"
      >
        Añadir alergia o intolerancia a tu lista
      </Button>
    </div>
  );
};

export default UserAllergensPage;
