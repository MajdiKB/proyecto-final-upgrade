import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
    homeButton1: {
        background: 'rgba(38,199,220,.8)',
        border: 0,
        borderRadius: 8,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white !Important',
        height: 50,
        padding: '0 30px',
        width: 350,
    },
    homeButton2: {
        background: '#CFCFCF',
        border: 0,
        borderRadius: 8,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white !Important',
        height: 50,
        padding: '0 30px',
        width: 350,
    },
    homeButton3: {
        background: '#FA7F9B',
        border: 0,
        borderRadius: 8,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white !Important',
        height: 50,
        padding: '0 30px',
        width: 350,
        cursor: 'pointer',
    },
    icons: {
        color: 'black',
        height: 40,
        width: 40,
        background: 'white',
    },
    homeIcon: {
        color: 'rgba(38,199,220,.8)',
        height: 40,
        width: 40,
        background: 'white',
    },
  });
  




export default  useStyles;
  